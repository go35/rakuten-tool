package main

import (
	"compress/gzip"
	"encoding/csv"
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/bitly/go-simplejson"
	"github.com/unknwon/goconfig"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	u "net/url"
	"time"
)

var (
	SoftDir string
	Transport *http.Transport
	Config *goconfig.ConfigFile
)

type CollyRekuten struct {
	GoodsFilename string
	Shopurl string
	Keywords string
	Itemids string
	Proxy string
	Maxpage int
	Client *http.Client
	Datas map[int]Goods
	Autocompletes map[string]string
}

type Goods struct {
	Shopurl string
	Keywords string
	Title string
	Url string
	Tr string
	Td string
	Page string
	Index string
	Itemid string
}

/**
根据URL获取内容
 */
func (this *CollyRekuten) GetDocument(url string) (*goquery.Document, error) {
	req, err := http.NewRequest("GET", url, strings.NewReader("name=rakuten"))
	if err != nil {
		fmt.Println("网络配置异常，程序退出")
		return nil, errors.New("网络配置异常，程序退出")
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	req.Header.Set("Referer", "https://www.rakuten.co.jp/")
	req.Header.Set("Accept-Encoding", "gzip, deflate")
	req.Header.Set("Accept-Language", "zh-CN,zh;q=0.8")

	resp, err := this.Client.Do(req)
	if err != nil {
		return nil, errors.New("网络返回异常，程序退出")
	}

	defer resp.Body.Close()

	var reader io.ReadCloser
	if resp.Header.Get("Content-Encoding") == "gzip" {
		reader, err = gzip.NewReader(resp.Body)
		if err != nil {
			fmt.Println("gzip解压异常，程序退出")
			return nil, errors.New("gzip解压异常，程序退出")
		}
	} else {
		reader = resp.Body
	}
	body, err := ioutil.ReadAll(reader)
	if err != nil {
		fmt.Println("Body读取异常，程序退出")
		return nil, errors.New("Body读取异常，程序退出")
	}
	html := strings.NewReader(string(body))
	return goquery.NewDocumentFromReader(html)
}

/**
初始化页数
 */
func (this *CollyRekuten) InitPage(url string, keyword string) int {
	doc, err := this.GetDocument(url)
	if err != nil {
		// 重新尝试一次
		doc, err = this.GetDocument(url)
		if err != nil {
			fmt.Println("尝试两次失败")
			return 0
		}
	}
	totalStr := doc.Find(".nav .count").Text()
	if totalStr == "" {
		fmt.Println("总数匹配错误，程序退出")
		return 0
	}
	totalStr = strings.Replace(totalStr, ",", "", -1)
	totalStrArr := strings.Split(totalStr, "（")
	re := regexp.MustCompile(`([0-9]+)`)
	total := re.FindString(totalStrArr[1])
	totalInt, _ := strconv.Atoi(total)
	page := int(math.Ceil(float64(totalInt) / float64(45)))
	if page > this.Maxpage {
		page = this.Maxpage
	}
	fmt.Printf("数据总数%d条，最大%d页 \n", totalInt, page)
	return page
}

func (this *CollyRekuten) Start() {
	//设置代理
	proxy, err := u.Parse(this.Proxy)
	if err != nil {
		fmt.Println("请设置代理，程序退出")
		os.Exit(0)
	}
	this.Client = &http.Client {
		Transport: &http.Transport {
			Proxy : http.ProxyURL(proxy),
		},
	}
	//开始采集
	baseurl := "https://search.rakuten.co.jp/search/mall/"
	//关键字联想数据匹配
	this.GetAutocomplete(this.Keywords)
	//Items转换为 Arr
	itemidsArr := strings.Split(this.Itemids, ",")
	//全局索引
	dindex := 1
	for keyword, _ := range this.Autocompletes {
		url := baseurl + u.QueryEscape(keyword)
		fmt.Println(url, keyword)
		//初始化最大页
		maxPage := this.InitPage(url, keyword)
		//遍历Maxpage页数据
		for i := 1; i <= maxPage; i++ {
			fmt.Printf("开始采集数据，关键字 (%s)，当前%d页 \n", keyword, i)
			var urlParams = u.Values{}
			urlParams.Add("p", strconv.Itoa(i))//当前页数
			doc, err := this.GetDocument(url + "?" + urlParams.Encode())
			if err != nil {
				fmt.Println(err.Error())
				fmt.Println("跳过本页")
				continue
			}
			doc.Find(".searchresultitems").Children().Each(func(i2 int, selection *goquery.Selection) {
				//id, _ := selection.Find(".image .favorite").Attr("data-id")
				title := selection.Find(".title a").Text()
				href, _ := selection.Find(".title a").Attr("href")
				if strings.Contains(href, strings.Trim(this.Shopurl, " ")) == true {
					index := i2 +1
					tr := math.Ceil( float64(index) / 7)
					td := index % 7
					if td == 0 {
						td = 7
					}
					for _, itemid := range itemidsArr {
						if strings.Contains(href, strings.Trim(itemid + "/", " ")) == true {
							this.Datas[dindex] = Goods{this.Shopurl, keyword, title, href, strconv.Itoa(int(tr)), strconv.Itoa(td), strconv.Itoa(i), strconv.Itoa(i2), itemid}
						}
					}
					dindex++
				}
			})
		}
	}
	this.SaveDatas()
}

func (this *CollyRekuten) SaveDatas() {
	// 保存商品信息
	fmt.Println(this.GoodsFilename)
	f, err := os.Create(this.GoodsFilename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString("\xEF\xBB\xBF") // 写入UTF-8 BOM
	w := csv.NewWriter(f)
	w.Write([]string{"店铺标识","关键字","商品id","商品标题","页","排名","行","列"})
	for _, item := range this.Datas {
		w.Write([]string{item.Shopurl, item.Keywords, item.Itemid, item.Title, item.Page, item.Index, item.Tr, item.Td})
	}
	w.Flush()
}

func (this *CollyRekuten) GetAutocomplete(keyword string) {

	baseurl := "https://rdc-api-catalog-gateway-api.rakuten.co.jp/SUI/autocomplete/search_pc?"
	var urlParams = u.Values{}
	urlParams.Add("q", keyword)
	urlParams.Add("rid", "6250760139")

	req, err := http.NewRequest("GET", baseurl + urlParams.Encode(), strings.NewReader("name=rakuten"))
	if err != nil {
		fmt.Println("网络配置异常，程序退出")
		os.Exit(0)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
	req.Header.Set("Referer", "https://www.rakuten.co.jp/")

	resp, err := this.Client.Do(req)
	if err != nil {
		fmt.Println("网络返回异常，程序退出")
		os.Exit(0)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Body读取异常，程序退出")
		os.Exit(0)
	}
	rejson, err := simplejson.NewJson(body)
	if err != nil {
		fmt.Println("关键字JSON转码失败", err)
		os.Exit(0)
	}
	suggs, _ := rejson.Get("suggestions").Array()
	for _, item := range suggs {
		if each_map, ok := item.(map[string]interface{}); ok {
			word := each_map["name"].(string)
			this.Autocompletes[word] = keyword
		}
	}
}

func init() {
	//软件目录获取
	SoftDir = "."
	if dir, err := os.Getwd(); err == nil {
		SoftDir = dir
	}
	//读取conf配置
	var err error
	if Config, err = goconfig.LoadConfigFile(SoftDir + "/conf.ini"); err != nil {
		fmt.Println("配置文件加载失败，程序退出")
		os.Exit(0)
	}
}

func main() {
	c := &CollyRekuten{}
	//初始化配置参数
	tstr := strconv.Itoa(time.Now().Year()) + strconv.Itoa(int(time.Now().Month())) + strconv.Itoa(time.Now().Day()) + strconv.Itoa(time.Now().Hour()) + strconv.Itoa(time.Now().Minute()) + strconv.Itoa(time.Now().Second())
	c.GoodsFilename = SoftDir + "/" + tstr + "_goods.csv"
	c.Datas = make(map[int]Goods)
	c.Autocompletes = make(map[string]string)
	c.Shopurl = Config.MustValue("config", "shopurl")
	c.Keywords = Config.MustValue("config", "keywords")
	c.Itemids = Config.MustValue("config", "itemids")
	c.Proxy = Config.MustValue("config", "proxy")
	c.Maxpage, _ = strconv.Atoi(Config.MustValue("config", "maxpage"))
	c.Start()
}
